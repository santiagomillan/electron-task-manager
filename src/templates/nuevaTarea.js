const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function crearVentanaNuevaTarea() {
  // Crea la ventana para crear una nueva tarea
  let ventana = new BrowserWindow({
    width: 800,
    height: 250,
    title: "Nueva Tarea",
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // Elimina el menú de la ventana para crear tarea
  ventana.setMenu(null);

  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "../views/nuevaTarea.html"),
    protocol: "file",
    slashes: true,
  });
  // Carga el HTML
  ventana.loadURL(urlVentana);

  // Libera memoria cuando se cierra la ventana
  ventana.on("closed", () => {
    ventana = null;
  });

  return ventana;
}

module.exports = { crearVentanaNuevaTarea };
