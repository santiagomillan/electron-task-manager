document.addEventListener("DOMContentLoaded", () => {
     // Obtiene una referencia al boton
     let boton = document.getElementById("boton");
     let error = document.getElementById("error");
  
//     // Click en el boton
     boton.addEventListener("click", (evento) => {
//       // Evita el envío del formulario
       evento.preventDefault();
//       // Obtiene el input del nombre
       let textNombre = document.getElementById("nombre");
//       // Valida si se digitó el nombre
       if (textNombre.value === "") {
//         // Modifica el estilo/
         textNombre.style.border = "2px solid red";
         textNombre.style.background = "pink";
         // Muestra el error
         error.style.display = "block";
       } else {
//         // Obtiene el formulario
         let formulario = document.getElementById("formulario");
//         // Envia el formulario
         formulario.submit();
       }
     });
  
  error.addEventListener("mouseover", () =>{
   console.log("Mouse sobre el error");
     });   
  error.addEventListener("mouseout", () =>{
   console.log("Mouse salió el error");
     });
   error.addEventListener("click", () =>{
     console.log("Click en el error");
  });
 });
