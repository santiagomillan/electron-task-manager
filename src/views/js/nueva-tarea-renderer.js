const { ipcRenderer } = require('electron');
// Click en el boton de crear tarea
boton.addEventListener("click", (evento) => {
    // Previene el comportamiento por defecto
    evento.preventDefault();
    // Obtiene el texto
    let inputTarea = document.getElementById("tarea");
    // Obtiene el texto digitado
    let tarea = inputTarea.value;
    // Crea objeto de datos que se enviaran
    const datos = {
        tarea: tarea
    };
    // envio de información al proceso principal
    ipcRenderer.send("nueva-tarea", datos);
});